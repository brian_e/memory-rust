const C: i32 = 54254;

fn main(){
    let x_ref: &i32 = &7;
    let y_box: Box<i32> = Box::new(34);
    let a_ref: &i32 = &475;
    let b_box: Box<i32> = Box::new(3631);

    let c_ref: &i32 = &C;

    foo(x_ref, y_box);

    println!("{:?} {:?}", a_ref, b_box);
    println!("{:?}", c_ref);
}

fn foo(x_ref: &i32, y_box: Box<i32>) {
    println!("{:?} {:?}", x_ref, y_box);
}
