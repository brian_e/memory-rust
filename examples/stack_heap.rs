fn foo(x: &i32) {
    let y = 10;
    let z = &y;

    baz(z);
    bar(x, z);
}

fn bar(a: &i32, b: &i32) {
    let c = 5;
    let d = Box::new(5);
    let e = &d;

    baz(e);
    println!("{:?} {:?} {:?}", a, b, c);
}

fn baz(f: &i32) {
    let g = 100;
    println!("{:?} {:?}", f, g);
}

fn main() {
    let h = 3;
    let i = Box::new(20);
    let j = &h;

    foo(j);
    println!("{:?}", i);
}
