# Debugging memory allocation in rust with gdb and lldb

* Book: [The Stack and the Heap](https://doc.rust-lang.org/book/the-stack-and-the-heap.html)
* Rust by Example: [Box, stack and heap](http://rustbyexample.com/std/box.html)


## gdb (vagrant-ubuntu-trusty-64)

    $ ./debug.sh gdb stack_heap
    
    (gdb) rbreak bar
    
    (gdb) run
    Starting program: /vagrant/memory-rust/target/debug/memory
    Breakpoint 1, memory::bar::h70e0e51c77002dd2 () at src/main.rs:14
    14      baz(e);
    
n n n s n

    20      println!("{:?} {:?}", f, g);

    (gdb) backtrace
    #0  memory::baz (f=0x7ffff6a14028) at src/main.rs:20
    #1  0x000055555555bb0e in memory::bar (a=0x7fffffffe314, b=0x7fffffffe234) at src/main.rs:14
    #2  0x000055555555b991 in memory::foo (x=0x7fffffffe314) at src/main.rs:6
    #3  0x000055555555bf39 in memory::main () at src/main.rs:28

    (gdb) info args
    f = 0x7ffff6a14028

    (gdb) p g
    $2 = 100
    (gdb) x/4b &g
    0x7fffffffe0c4: 100 0   0   0

    (gdb) p f
    $1 = (i32 *) 0x7ffff6a14028
    (gdb) p *f
    $3 = 5
    (gdb) x/4b f
    0x7ffff6a14028: 5   0   0   0


    up

    (gdb) frame
    #1  0x000055555555bb0e in memory::bar (a=0x7fffffffe314, b=0x7fffffffe234) at src/main.rs:14
    14      baz(e);

    (gdb) info args
    a = 0x7fffffffe314
    b = 0x7fffffffe234

    (gdb) info locals
    c = 5
    d = 0x7ffff6a14028
    e = 0x7fffffffe1e8


    (gdb) info frame
    Stack level 1, frame at 0x7fffffffe220:
     rip = 0x55555555bb0e in memory::bar (src/main.rs:14); saved rip = 0x55555555b991
     called by frame at 0x7fffffffe250, caller of frame at 0x7fffffffe0e0
     source language minimal.
     Arglist at 0x7fffffffe210, args: a=0x7fffffffe314, b=0x7fffffffe234
     Locals at 0x7fffffffe210, Previous frame's sp is 0x7fffffffe220
     Saved registers:
      rbp at 0x7fffffffe210, rip at 0x7fffffffe218


    (gdb) p d
    $4 = (i32 *) 0x7ffff6a14028
    (gdb) p *d
    $6 = 5
    (gdb) x/4b d
    0x7ffff6a14028: 5   0   0   0


## lldb (vagrant-ubuntu-trusty-64)

    $ sudo apt-get install -y clang-3.6 lldb-3.6
    $ sudo update-alternatives --install /usr/bin/clang clang /usr/bin/clang-3.6 50 \
                               --slave /usr/bin/clang++ clang++ /usr/bin/clang++-3.6 \
                               --slave /usr/bin/lldb lldb /usr/bin/lldb-3.6

* [LLDB Tutorial](http://lldb.llvm.org/tutorial.html)

    $ ./debug.sh lldb stack_heap
    (lldb) process launch --stop-at-entry --environment MallocStackLogging=1
    (lldb) breakpoint set -n bar
    (lldb) thread continue

Repeat this command until the call to baz inside bar

    (lldb) thread step-over

Step into baz from bar

    (lldb) thread step-in

Step over the assignment to g

    (lldb) thread step-over
       17   fn baz(f: &i32) {
       18       let g = 100;
    -> 20       println!("{:?} {:?}", f, g);
       21   }

    (lldb) thread backtrace
      * frame #0: 0x00000001000014c2 memory`memory::baz(f=&0x100616020) + 34 at main.rs:20
        frame #1: 0x000000010000160e memory`memory::bar(a=&0x7fff5fbff164, b=&0x7fff5fbff084) + 78 at main.rs:14
        frame #2: 0x0000000100001491 memory`memory::foo(x=&0x7fff5fbff164) + 49 at main.rs:6
        frame #3: 0x0000000100001a39 memory`memory::main + 89 at main.rs:28

    (lldb) frame variable
    (int *) f = &0x100616020
    (int) g = 100

    (lldb) frame variable *f
    (int) *f = 5

    (lldb) up
    frame #1: 0x000000010000160e memory`memory::bar(a=&0x7fff5fbff164, b=&0x7fff5fbff084) + 78 at main.rs:14
       11       let d = Box::new(5);
       12       let e = &d;
       13
    -> 14       baz(e);
       15       println!("{:?} {:?} {:?}", a, b, c);
       16   }
       17

    (lldb) frame variable
    (int *) a = &0x7fff5fbff164
    (int *) b = &0x7fff5fbff084
    (int) c = 5
    (int *) d = &0x100616020
    (int **) e = &0x7fff5fbff038


## Comand history

    history | awk '{ print substr($0, index($0,$2)) }' | sort | uniq


### Mac

    git clone <this repo>

    vagrant init ubuntu/trusty64
    vagrant up --provider virtualbox
    vagrant ssh


### VM

    cd /vagrant/memory-rust/

    sudo apt-get update
    sudo apt-get autoremove
    sudo apt-get install -y build-essential gdb clang-3.6 llvm-3.6 lldb-3.6
    sudo update-alternatives --install /usr/bin/clang clang /usr/bin/clang-3.6 50 \
                             --slave /usr/bin/clang++ clang++ /usr/bin/clang++-3.6  \
                             --slave /usr/bin/lldb lldb /usr/bin/lldb-3.6

    curl https://sh.rustup.rs -sSf | sh
    source $HOME/.cargo/env
    rustup default nightly

    ./debug.sh gdb <example name>
    ./debug.sh lldb <example name>
