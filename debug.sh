#! /bin/bash

set -eux

DEBUGGER="${1}" # gdb or lldb
NAME="${2}"

cargo build --example "${NAME}"
"rust-${DEBUGGER}" "target/debug/examples/${NAME}"
